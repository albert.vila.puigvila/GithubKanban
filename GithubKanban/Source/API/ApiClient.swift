//
//  ApiClient.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import Foundation


typealias Results<T> = (_ elements:[T]?, _ error: Error?) -> Void


class ApiClient {
    
    static let shared = ApiClient()
    
    let urlString = "https://api.github.com/repositories?since=10"
    
    func getRepositories(completion:@escaping Results<GHRepo>) {
        let url = URL(string: urlString)!
        
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if let error = error  {
                completion(nil, ErrorHandling.unknown(error))
                return
            }
            guard let content = data else {
                completion(nil, ErrorHandling.noData)
                return
            }

            let repos = GHRepoProvider.decodeArray(data: content)
            completion(repos, nil)
        }
        task.resume()
    }
}
