//
//  VCManager.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit


enum VCManager {
    
    case repoList
    
    case repoDetail
    case backlog
    case next
    case doing
    case done
}

extension VCManager: RawRepresentable {
    
    typealias RawValue = UIViewController
    
    var rawValue: RawValue {
        switch self {
            case .repoList: let vc:RepoListViewController = .fromNib()
        return vc
            case .repoDetail: let vc:RepoDetailViewController = .fromNib()
        return vc
        case .backlog:
        return BacklogViewController()
        case .next:
        return NextViewController()
        case .doing:
        return DoingViewController()
        case .done:
        return DoneViewController()
        }
    }
    
    init?(rawValue: RawValue) {
        switch rawValue {
        default: return nil
        }
    }
}
