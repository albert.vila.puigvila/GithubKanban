//
//  AppCoordinator.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}


class AppCoordinator: Coordinator {
    
    let window: UIWindow
    let controller:UIViewController
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    
    // MARK: - Life Cycle -
    init(window: UIWindow, controller: UIViewController) {
        
        self.window     = window
        self.controller = controller
        self.navigationController = UINavigationController(rootViewController: controller)
    }
    
    func start() {
        self.navigationController.viewControllers = [controller]
        self.window.rootViewController = navigationController
        self.window.makeKeyAndVisible()
        controller.title = "GH Kanban"
    }
    
//    private func buildIfUserIsLogged() {
//        let coordinator    = SelectLoginCoordinator(presenter: navigationController)
//        let selectedLogged = coordinator.selectedLoggedControler
//        let dashboard      = VCManager.dashboard.rawValue as! DashboardViewController
//        navigationController.viewControllers = [selectedLogged, dashboard]
//    }
}
