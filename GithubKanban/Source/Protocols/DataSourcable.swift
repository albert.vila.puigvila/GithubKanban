//
//  DataSourcable.swift
//  GithubKanban
//
//  Created by albert vila on 03/05/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit


protocol DataSourcable {
    associatedtype Items
    
    var items:[Items] { get set }
    init(controller: UIViewController, tableView:UITableView, items: [Items])
    
    func reload(new items: [Items])
}

extension DataSourcable {
    init(controller: UIViewController, tableView:UITableView, items: [Items] = []) {
        self.init(controller: controller, tableView: tableView, items: items)
    }
}
