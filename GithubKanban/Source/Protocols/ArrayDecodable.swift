//
//  ArrayDecodable.swift
//  GithubKanban
//
//  Created by albert vila on 30/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import Foundation


protocol ArrayDecodable {
    associatedtype Entity
}

// MARK: - Decode Function -
extension ArrayDecodable {
    
    @discardableResult
    static func decodeArray(data: Data) -> [Entity]? {
        let decoder = JSONDecoder()
        guard let results = try? decoder.decode([Entity].self, from: data) else {
            return nil
        }
        return results
    }
}
