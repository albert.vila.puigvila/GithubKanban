//
//  LoadableData.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import Foundation

protocol LoadableData {
    associatedtype Data
    
    func loadData(data: Data)
}
