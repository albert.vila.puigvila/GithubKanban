//
//  JSON+Extras.swift
//  CoreDataStack
//
//  Created by albert vila on 21/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//


import UIKit

public extension JSON {
    
    public func primaryKey(_ key:String) -> String? {
        return !self[key].stringValue.isEmpty ? self[key].stringValue : nil
    }
    
    public func stringForKey(_ key:String) -> String {
        return self[key].stringValue
    }
    
    public func boolForKey(_ key:String) -> Bool {
        return self[key].boolValue
    }
    
    public func intForKey(_ key:String) -> Int {
        return self[key].intValue
    }
    
    public func floatForKey(_ key:String) -> CGFloat {
        return CGFloat(self[key].floatValue)
    }
    
    public func doubleForKey(_ key:String) -> Double {
        return self[key].doubleValue
    }
    
    public func jsonForKey(_ key:String) -> JSON {
        return self[key]
    }
}
