//
//  UITableView+Extras.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit

extension UITableView {
    
    func registerCustomCell<T: UITableViewCell>(type: T.Type) {
        self.register(T.self, forCellReuseIdentifier: T.className)
    }
    
    func registerCustomCellNib<T: UITableViewCell>(type: T.Type) {
        self.register(UINib(nibName: T.className, bundle: nil), forCellReuseIdentifier: T.className)
    }
    
    func dequeueReusableCell<T:UITableViewCell>(type: T.Type) -> T? {
        let fullName = T.className
        return self.dequeueReusableCell(withIdentifier: fullName) as? T
    }
    
    func dequeueReusableCell<T:UITableViewCell>(type: T.Type, forIndexPath indexPath:IndexPath) -> T? {
        let fullName = T.className
        return self.dequeueReusableCell(withIdentifier: fullName, for:indexPath as IndexPath) as? T
    }
}


// MARK: - Helper's -
extension NSObject {
    public static var className: String {
        return String(describing: self)
    }
}
