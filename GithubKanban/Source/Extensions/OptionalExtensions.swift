//
//  OptionalExtensions.swift
//  CoreDataStack
//
//  Created by albert vila on 25/03/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import Foundation

public extension Optional {
    
    func then(_ handler: (Wrapped) -> Void) {
        switch self {
        case .some(let wrapped): return handler(wrapped)
        case .none: break
        }
    }
    
    @discardableResult
    func ifSome(_ handler: (Wrapped) -> Void) -> Optional {
        switch self {
        case .some(let wrapped): handler(wrapped); return self
        case .none: return self
        }
    }
    
    @discardableResult
    func ifNone(_ handler: () -> Void) -> Optional {
        switch self {
        case .some: return self
        case .none: handler(); return self
        }
    }
}
