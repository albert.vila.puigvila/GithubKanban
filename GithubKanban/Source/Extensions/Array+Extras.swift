//
//  Array+Extras.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import Foundation

extension Array {
    func get(at index: Int) -> Element? {
        guard index >= 0 && index < count else { return nil }
        return self[index]
    }
}
