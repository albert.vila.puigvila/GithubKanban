//
//  UIView+Extras.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit

extension UIView {
    
    func setBorder(width: CGFloat, color: UIColor = .black, radius:CGFloat = 0) {
        layer.borderWidth  = width
        layer.borderColor  = color.cgColor
        layer.cornerRadius = radius
    }
}
