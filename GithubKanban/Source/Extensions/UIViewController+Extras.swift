//
//  UIViewController+Extras.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit


public extension UIViewController {
    
    class func fromNib<T : UIViewController>() -> T {
        return T(nibName:String(describing: T.self), bundle: nil)
    }
}
