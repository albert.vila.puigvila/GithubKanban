//
//  RepoListCellTableViewCell.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit

class RepoListCellTableViewCell: UITableViewCell {

    @IBOutlet private weak var container: UIView! {
        didSet { container.setBorder(width: 1, radius: 2.5) }
    }
    @IBOutlet private weak var lbRepoName: UILabel!
    @IBOutlet private weak var lbRepoAuthor: UILabel!
    
    fileprivate var repo:GHRepo?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    @IBAction func actAddRepo(_ sender: Any) {
        if let repo = repo {
            GHRepoDataManager.shared.createOrUpdateRepos(from: [repo], handle: { error in
                error.ifSome {
                    print($0.localizedDescription)
                }
            })
        }
    }
}

extension RepoListCellTableViewCell: LoadableData {
    typealias Data = GHRepo
    
    func loadData(data: GHRepo) {
        self.repo = data
        
        lbRepoName.text   = data.name
        lbRepoAuthor.text = data.author
    }
}
