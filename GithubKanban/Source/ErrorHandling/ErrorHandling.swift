//
//  ErrorHandling.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import Foundation

enum ErrorHandling: Error {
    case unknown(Error)
    case noData
}
