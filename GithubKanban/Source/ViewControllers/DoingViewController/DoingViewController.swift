//
//  DoingViewController.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit

class DoingViewController:UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .green
    }
}
