//
//  RepoListDataSource.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit


class RepoListDataSource: NSObject, DataSourcable {
    typealias Items = GHRepo
    
    var items: [Items]
    
    fileprivate let controller: UIViewController
    fileprivate let tableView: UITableView

    required init(controller: UIViewController, tableView: UITableView, items: [Items]) {
        
        self.controller = controller
        self.tableView  = tableView
        self.items      = !items.isEmpty ? items : []
        
        super.init()
        DispatchQueue.main.async {
            self.setDelegateAndDataSource()
            self.registerCells()
        }
    }
    
    private func setDelegateAndDataSource() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func registerCells() {
        tableView.registerCustomCellNib(type: RepoListCellTableViewCell.self)
    }
    
    func reload(new items: [GHRepo] = []) {
        DispatchQueue.main.async {
            self.items = items
            self.tableView.reloadData()
        }
    }
}

// MARK: - UITableViewDataSource -
extension RepoListDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kScreenHeight * 0.175
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(type: RepoListCellTableViewCell.self, forIndexPath: indexPath) else {
            return UITableViewCell()
        }
        if let repo = items.get(at: indexPath.row) {
            cell.loadData(data: repo)
        }
        return cell
    }
}

// MARK: - UITableViewDelegate -
extension RepoListDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let detailCoordinator = RepoDetailCoordinator(presenter: controller.navigationController!, controller: controller)
        detailCoordinator.start()
    }
}
