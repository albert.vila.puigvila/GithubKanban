//
//  RepoListViewController.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit
import CoreData

class RepoListViewController: UIViewController {
    
    private enum ReposFrom: Int {
        case explore
        case local
    }
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet { tableView.separatorStyle = .none }
    }
    @IBOutlet private weak var segControl: UISegmentedControl!
    
    private var dataSource: RepoListDataSource? = nil
    private var reposFromApi:[GHRepo]   = [GHRepo]()
    private var reposFavorites:[GHRepo] = [GHRepo]()
    
    
    // MARK: - Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getRepositoriesFromApi()
    }
    
    private func getRepositoriesFromApi(stored:[GHRepo]? = nil) {
        self.dataSource = RepoListDataSource(controller: self, tableView: self.tableView)
        
        if let stored = stored {
            self.dataSource?.reload(new: stored)
        }
        
        ApiClient.shared.getRepositories { [weak self] repos, error in
            error.then { _ in
                return
            }
            guard let reposGitHub = repos else {
                return
            }
            self?.reposFromApi = reposGitHub
            self?.dataSource?.reload(new: reposGitHub)
        }
    }
    
    @IBAction func actSegControl(_ sender: UISegmentedControl) {
        guard let dataFrom = ReposFrom(rawValue: sender.selectedSegmentIndex) else {
            return
        }
        
        switch dataFrom {
        case .explore:
            self.dataSource?.reload(new: reposFromApi)
        case .local:
            GHRepoDataManager.shared.fetchAllRepos(handler: { repos, error in
                repos.ifNone {
                    self.reposFavorites = []
                }.ifSome {
                    self.reposFavorites = $0
                }
                self.dataSource?.reload(new: self.reposFavorites)
            })
        }
    }    
}
