//
//  RepoListViewModel.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit

class RepoListViewModel {
    
    let name:String?
    var author:String?
    
    init(repo: GHRepo) {
        name   = repo.name
        author = repo.author
    }
}
