//
//  RepoDetailCoordinator.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit

class RepoDetailCoordinator: Coordinator {
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = [Coordinator]()
    
    private let controller:UIViewController
    
    
    // MARK: - Life Cycle -
    init(presenter: UINavigationController, controller: UIViewController) {
        self.navigationController = presenter
        self.controller           = controller
    }
    
    func start() {
        let detail = VCManager.repoDetail.rawValue as! RepoDetailViewController
        detail.title = "GH Kanban"
        navigationController.pushViewController(detail, animated: true)
    }
}
