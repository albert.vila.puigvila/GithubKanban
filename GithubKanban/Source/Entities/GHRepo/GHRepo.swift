//
//  GHRepo.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import UIKit
import CoreData


struct GHRepo {
    let id:String?
    let name:String?
    let author:String?
    
    init(id: String?, name: String?, author: String?) {
        self.id     = id
        self.name   = name
        self.author = author
    }
}

extension GHRepo: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case owner
        case login
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let owner  = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .owner)

        id     = try? String(values.decode(Int.self, forKey: .id))
        name   = try? values.decode(String.self, forKey: .name)
        author = try? owner.decode(String.self, forKey: .login)
    }
}


extension GHRepo: ManagedObjectConvertible {
    typealias ManagedObject = GHRepoEntity
    
    func toManagedObject(in context: NSManagedObjectContext) -> ManagedObject? {
        guard let primaryKey = id else {
            return nil
        }
        let ghRepo = GHRepoEntity.getOrCreateSingle(primaryKey: primaryKey, from: context)
        ghRepo.setValue(name, forKey: "name")
        ghRepo.setValue(author, forKey: "author")
        return ghRepo
    }
}
