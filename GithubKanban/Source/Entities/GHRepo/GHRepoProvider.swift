//
//  GHRepoProvider.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import Foundation


struct GHRepoProvider: ArrayDecodable {
    typealias Entity = GHRepo
    
    static func all(handle:@escaping (_ results:[GHRepo]?, _ error: Error?) -> Void) {
        CoreDataStack.shared.performForegroundTask { context in
            do {
                let fetchRequest = GHRepoEntity.fetchRequest()
                
                let results = try context.fetch(fetchRequest) as? [GHRepoEntity]
                let list = results?.flatMap { $0.toEntity() } ?? []
                handle(list, nil)
            } catch {
                handle(nil, error)
            }
        }
    }
    
}
