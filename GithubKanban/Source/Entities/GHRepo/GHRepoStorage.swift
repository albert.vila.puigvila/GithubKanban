//
//  GHRepoStorage.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import Foundation


typealias GHRepoResponse = (_ repos: [GHRepo]?, _ error: Error?) -> Void
typealias GHRepoSaveResponse = (_ error: Error?) -> Void

protocol GHRepoDataManagerProtocol {
    func createOrUpdateRepos(from values: [GHRepo]?, handle:@escaping GHRepoSaveResponse)
    func fetchAllRepos(handler:@escaping GHRepoResponse)
}


// MARK: - GHRepoDataManager -
class GHRepoDataManager: GHRepoDataManagerProtocol {
    
    static let shared = GHRepoDataManager()
    
    private let worker: CoreDataWorker<GHRepoEntity, GHRepo>
    
    init(worker: CoreDataWorker<GHRepoEntity, GHRepo> = CoreDataWorker<GHRepoEntity, GHRepo>()) {
        self.worker = worker
    }
    
    func createOrUpdateRepos(from values: [GHRepo]?, handle:@escaping GHRepoSaveResponse) {
        guard let list = values, !list.isEmpty else {
            handle(ErrorsCoreData.emptyList)
            return
        }
        worker.insert(entities: list) { error in
            handle(error)
        }
    }
    
    func fetchAllRepos(handler: @escaping GHRepoResponse) {
        worker.fetch { (results: FetchResult<[GHRepo]>) in
            switch results {
            case .success(let repos):
                handler(repos, nil)
            case .failure(let error):
                handler(nil, error)
            }
        }
    }
}
