//
//  CoreDataStack.swift
//  CoreDataStack
//
//  Created by albert vila on 24/03/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import CoreData


protocol CoreDataStackProtocol: class {
//    associatedtype ModelName
    
    var errorHandler: (Error) -> Void { get set }
//    var modelName:ModelName { get }
    var persistentContainer: NSPersistentContainer { get }
    var viewContext: NSManagedObjectContext { get }
    var backgroundContext: NSManagedObjectContext { get }
    
    func performBackgroundTask(_ block: @escaping (NSManagedObjectContext) -> Void)
    func performForegroundTask(_ block: @escaping (NSManagedObjectContext) -> Void)
}

extension CoreDataStackProtocol { }


// MARK: - CoreDataStack -
final class CoreDataStack: CoreDataStackProtocol {
    typealias ModelName = String
    
    static let shared = CoreDataStack()
    
    var errorHandler: (Error) -> Void = { _ in }
    
    private var modelName: String {
        return "GithubKanban"
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let persistentContainer = NSPersistentContainer(name: self.modelName)
        persistentContainer.loadPersistentStores { _, error in
            if let error = error {
                fatalError("Failed to load Core Data stack: \(error)")
            }
        }
        return persistentContainer
    }()
    
    lazy var viewContext: NSManagedObjectContext = {
        let context:NSManagedObjectContext = self.persistentContainer.viewContext
        context.automaticallyMergesChangesFromParent = true
        return context
    }()
    
    lazy var backgroundContext: NSManagedObjectContext = {
        return self.persistentContainer.newBackgroundContext()
    }()
    
    func performForegroundTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        self.viewContext.perform {
            block(self.viewContext)
        }
    }
    
    func performBackgroundTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        self.persistentContainer.performBackgroundTask(block)
    }
    
    deinit {
        do {
            try self.viewContext.save()
        } catch {
            fatalError("Error deinitializing main managed object context")
        }
    }
}
