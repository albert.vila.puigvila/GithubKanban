//
//  GHRepoEntity+CoreDataProperties.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//
//

import Foundation
import CoreData


extension GHRepoEntity {

    @nonobjc public class func fetchRequesting() -> NSFetchRequest<GHRepoEntity> {
        return NSFetchRequest<GHRepoEntity>(entityName: "GHRepoEntity")
    }

    @NSManaged public var primaryKey: String?
    @NSManaged public var name: String?
    @NSManaged public var author: String?

}
