//
//  GHRepoEntity+CoreDataClass.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//
//

import Foundation
import CoreData


public class GHRepoEntity: NSManagedObject { }

extension GHRepoEntity: ManagedObjectProtocol {
    typealias Entity = GHRepo
    
    static var primaryKey: String {
        return "primaryKey"
    }
    
    func toEntity() -> GHRepo? {
        return GHRepo(id: primaryKey, name: name, author: author)
    }
}
