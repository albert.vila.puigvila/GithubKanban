//
//  FetchEntityProtocol.swift
//  CoreDataStack
//
//  Created by albert vila on 25/03/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import Foundation
import CoreData


enum FetchResult<T> {
    case success(T)
    case failure(Error)
}


// MARK: - Fetchable -
protocol Fetchable {
    static var primaryKey: String { get }
}

extension Fetchable  {
    static var entityName: String {
        return String(describing: self)
    }
}


// MARK: - ManagedObjectProtocol -
protocol ManagedObjectProtocol: Fetchable {
    associatedtype Entity
    
    func toEntity() -> Entity?
}

extension ManagedObjectProtocol where Self: NSManagedObject {
        
    @discardableResult
    static func getOrCreateSingle(primaryKey: String, from context: NSManagedObjectContext) -> Self {
        let result = single(from: context, with: primaryKey) ?? insertNew(in: context)
        result.setValue(primaryKey, forKey: self.primaryKey)
        return result
    }
    
    static func single(from context: NSManagedObjectContext, with predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?) -> Self? {
        return fetch(from: context, with: predicate, sortDescriptors: sortDescriptors, fetchLimit: 1)?.first
    }
    
    static func single(from context: NSManagedObjectContext, with id: String) -> Self? {
        let predicate = NSPredicate(format: "%K like %@", self.primaryKey, id)
        return single(from: context, with: predicate, sortDescriptors: nil)
    }
    
    static func insertNew(in context: NSManagedObjectContext) -> Self {
        return Self(context:context)
    }
    
    static func fetch(from context: NSManagedObjectContext, with predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil, fetchLimit: Int? = nil) -> [Self]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.predicate = predicate
        fetchRequest.returnsObjectsAsFaults = false
        
        if let fetchLimit = fetchLimit {
            fetchRequest.fetchLimit = fetchLimit
        }
        
        var result: [Self]?
        context.performAndWait { () -> Void in
            do {
                result = try context.fetch(fetchRequest) as? [Self]
            } catch {
                result = nil
                print("fetch error \(error)")
            }
        }
        return result
    }
    
    static func allObjects(from context: NSManagedObjectContext) -> [Self]? {
        return fetch(from: context)
    }
    
    @discardableResult
    static func deleteAll(from context: NSManagedObjectContext) throws -> ErrorsCoreData? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.includesPropertyValues = false
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try context.execute(batchDeleteRequest)
        } catch {
            throw ErrorsCoreData.cantDeleteAll(error)
        }
        return nil
    }
    /*
     static func fetchEntity(for id: String) -> Self? {
     guard let request = Self.fetchRequest() as? NSFetchRequest<Self> else {
     return nil
     }
     request.predicate  = NSPredicate(format: "primaryKey == %@", id)
     
     do {
     return try context.fetch(request).first
     } catch let error as NSError {
     print(error.debugDescription)
     return nil
     }
     }*/
}


enum ErrorsCoreData: Error {
    case cantDeleteAll(Error)
    case cannotSave(Error)
    case emptyList
    case save(Bool)
}
























