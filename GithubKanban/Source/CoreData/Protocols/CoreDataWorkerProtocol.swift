//
//  CoreDataWorkerProtocol.swift
//  GithubKanban
//
//  Created by albert vila on 01/05/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import CoreData

enum CoreDataWorkerError: Error{
    case cannotFetch(String)
    case cannotSave(Error)
}

protocol CoreDataWorkerProtocol {
    associatedtype EntityType
    
    func fetch(with predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?, fetchLimit: Int?, completion: @escaping (FetchResult<[EntityType]>) -> Void)
    func insert(entities: [EntityType], completion: @escaping (Error?) -> Void)
}

extension CoreDataWorkerProtocol {
    func fetch(with predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil, fetchLimit: Int? = nil, completion: @escaping (FetchResult<[EntityType]>) -> Void) {
        fetch(with: predicate, sortDescriptors: sortDescriptors, fetchLimit: fetchLimit, completion: completion)
    }
}


class CoreDataWorker<ManagedEntity, Entity>: CoreDataWorkerProtocol where ManagedEntity: NSManagedObject, ManagedEntity: ManagedObjectProtocol, Entity: ManagedObjectConvertible {
    
    let coreData: CoreDataStackProtocol
    
    init(coreData: CoreDataStackProtocol = CoreDataStack.shared) {
        self.coreData = coreData
    }
    
    func fetch(with predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]?, fetchLimit: Int?, completion: @escaping (FetchResult<[Entity]>) -> Void) {
        coreData.performForegroundTask { (context) in
            do {
                let fetchRequest = ManagedEntity.fetchRequest()
                fetchRequest.predicate = predicate
                fetchRequest.sortDescriptors = sortDescriptors
                if let fetchLimit = fetchLimit {
                    fetchRequest.fetchLimit = fetchLimit
                }
                let results = try context.fetch(fetchRequest) as? [ManagedEntity]
                let items: [Entity] = results?.flatMap { $0.toEntity() as? Entity } ?? []
                completion(.success(items))
            } catch {
                let fetchError = CoreDataWorkerError.cannotFetch("Cannot fetch error: \(error))")
                completion(.failure(fetchError))
            }
        }
    }
    
    func insert(entities: [Entity], completion: @escaping (Error?) -> Void) {
        coreData.performBackgroundTask { (context) in
            _ = entities.flatMap({ (entity) -> ManagedEntity? in
                return entity.toManagedObject(in: context) as? ManagedEntity
            })
            
            do {
                try context.save()
                completion(nil)
            } catch {
                completion(CoreDataWorkerError.cannotSave(error))
            }
        }
    }

}
