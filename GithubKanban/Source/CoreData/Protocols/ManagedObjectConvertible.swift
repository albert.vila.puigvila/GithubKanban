//
//  ManagedObjectConvertible.swift
//  GithubKanban
//
//  Created by albert vila on 25/04/2018.
//  Copyright © 2018 albert vila. All rights reserved.
//

import CoreData



// MARK: - ManagedObjectConvertible -
protocol ManagedObjectConvertible {
    associatedtype ManagedObject: NSManagedObject, ManagedObjectProtocol
    
    func toManagedObject(in context: NSManagedObjectContext) -> ManagedObject?
}

